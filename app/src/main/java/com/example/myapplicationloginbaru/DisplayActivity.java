package com.example.myapplicationloginbaru;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayActivity extends AppCompatActivity {
    private TextView displayText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        displayText = findViewById(R.id.displayText1);

        String username = getIntent().getStringExtra("username");
        String password = getIntent().getStringExtra("password");

        displayText.setText("Username: " + username + "\nPassword: " + password);
    }
}
